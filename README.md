# Base projet licence

## Consignes initiales

- Travailler sur votre dépot GIT avec la structure vue en cours
- A l'aide de prépros et de boostrap, créer une page avec un menu basique
    - Utiliser Boostrap 3 en LESS
    - Appeler jQuery sur votre page pour afficher "hello world" en titre de votre page
- Télécharger et installer NodeJS
    - Une fois installer executer dans votre terminal `npm -v`
    - Si une version est retournée, l'installation est opérationnelle. Sinon :
        - Fermer la session windows et se reconnecter.
        - Essayer a nouveau `npm -v`
- Dans le dossier du projet, executer les commandes suivantes dans votre terminal :
	- Attention au path, les commandes doivent être executées à la racine de votre projet !
    - `npm init`, passer les questions du terminal (entrer) puis à la fin `yes`
    - `npm install jquery`
    - `npm install bootstrap@3`
    - Ouvrir le fichier `package.json` et observer le document `node_modules`.
- Supprimer le document `node_modules` puis executer `npm i`
    - `npm update bootstrap`
    - Observer le fichier `package.json`. Que s'est-il passé ?
- Supprimer le `package.json` et `node_modules`
    - `npm init --yes`
    - `npm i owl.carousel`
    - Observer le fichier `package.json` puis `node_modules`. Que s'est-il passé ?
        
Une fois les consignes initiales éprouvées, réaliser les consignes suivantes sans télécharger de packages autrement qu'avec npm

- Supprimer le `package.json` et `node_modules` puis initier un nouveau package.json
- À l'aide de prepros, réaliser en un seul fichier css et un seul fichier js un page d'accueil avec une navigation principale et un slider à l'aide de OwlCarousel2
- Avec LESS, changer des variables bootstrap pour un nav sans bords arrondis : [contre exemple](https://codepen.io/davidcochran/pen/whdyr)
- Attention a vos path d'import (css & js).
       
## Quelques commandes NPM

- `npm init` ou `npm init --yes` ou `npm init -y`
- `npm install` ou `npm i`
- `npm i nom-du-package`
- `npm i nom-du-package --save`
- `npm i nom-du-package --save-dev`
- `git uninstall`

## Quelques commandes GIT

Client Git gratuit et cross-plateform : [SourceTree](https://www.sourcetreeapp.com/)

- `git status`
- `git add path/du/fichier.ext`
- `git add "test/*.ext"` : ajoute l'ensemble des fichiers *.ext du dossier **test** 
- `git commit` : sans le paramètre `-m` un éditeur VIM s'ouvre :
	* `i` pour le mode insertion
	* `:wq` pour quitter
- `git commit -m "nom de votre commit"`
- `git commit --all -m "nom de votre commit"` : ajoute l'ensemble des fichiers modifiés automatiquement.
- `git log --graph` : Pour voir le graph
- `git log --all --decorate --oneline --graph` : Comme la commande précedente, en plus compact.
- `git help COMMAND` : ouvre la documentation sur une commande git dans votre navigateur.


